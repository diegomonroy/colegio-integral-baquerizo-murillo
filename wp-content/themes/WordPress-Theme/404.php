<?php get_header( 'popup' ); ?>
	<!-- Begin 404 -->
		<section class="cuatro_cero_cuatro" data-wow-delay="0.5s">
			<div class="row collapse expanded">
				<div class="small-12 columns">
					<p class="text-center"><img src="<?php echo get_site_url(); ?>/wp-content/themes/WordPress-Theme/build/logo.svg" title="<?php bloginfo(title); ?>" alt="<?php bloginfo(title); ?>"></p>
					<h1 class="text-center">
						<span class="line_1">ERROR</span><br />
						<span class="line_2">404</span>
					</h1>
					<h2 class="text-center">Página no encontrada</h2>
					<h3 class="text-center"><a href="<?php echo get_site_url(); ?>">Regresar al inicio</a></h3>
				</div>
			</div>
		</section>
	<!-- End 404 -->
<?php get_footer( 'popup' ); ?>