		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-2' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php get_template_part( 'part', 'block-4' ); ?>
		<?php get_template_part( 'part', 'block-5' ); ?>
		<?php get_template_part( 'part', 'block-6' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php dynamic_sidebar( 'proceso_de_admision' ); ?>
		<?php dynamic_sidebar( 'contactenos' ); ?>
		<?php wp_footer(); ?>
		<?php if ( is_front_page() ) : ?>
		<div id="hidden-content" style="max-width: 800px; display: none;">
			<?php dynamic_sidebar( 'pop_up' ); ?>
		</div>
		<script>
			jQuery( document ).ready(function () {
				jQuery.fancybox.open({
					src: '#hidden-content',
					touch: false,
					smallBtn: false,
					opts: {
						afterShow: function () {
							setTimeout(function () {
								jQuery.fancybox.close();
							}, 10000);
						}
					}
				});
			});
		</script>
		<?php endif; ?>
	</body>
</html>