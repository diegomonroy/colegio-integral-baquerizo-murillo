<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php /*if ( is_page( array( 'por-que-integral' ) ) ) : dynamic_sidebar( 'banner_por_que_integral' ); endif;*/ ?>
				<?php /*if ( is_page( array( 'que-nos-diferencia' ) ) ) : dynamic_sidebar( 'banner_que_nos_diferencia' ); endif;*/ ?>
				<?php if ( is_page( array( 'soluciones-academicas' ) ) ) : dynamic_sidebar( 'banner_soluciones_academicas' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->