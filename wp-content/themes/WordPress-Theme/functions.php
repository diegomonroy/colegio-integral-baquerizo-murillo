<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Top 2',
			'id' => 'top_2',
			'before_widget' => '<div class="moduletable_to21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Menu',
			'id' => 'menu',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => '25 Years',
			'id' => '25_years',
			'before_widget' => '<div class="moduletable_to3 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Por Qué Integral',
			'id' => 'banner_por_que_integral',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Qué Nos Diferencia',
			'id' => 'banner_que_nos_diferencia',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Soluciones Académicas',
			'id' => 'banner_soluciones_academicas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 5',
			'id' => 'block_5',
			'before_widget' => '<div class="moduletable_b51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 6',
			'id' => 'block_6',
			'before_widget' => '<div class="moduletable_b61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Proceso de Admisión',
			'id' => 'proceso_de_admision',
			'before_widget' => '<div class="moduletable_pda1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Contáctenos',
			'id' => 'contactenos',
			'before_widget' => '<div class="moduletable_con1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Contáctenos Móvil',
			'id' => 'contactenos_movil',
			'before_widget' => '<div class="moduletable_conm1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Pop Up',
			'id' => 'pop_up',
			'before_widget' => '<div class="moduletable_pop1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 220,
		'height' => 220,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 10;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	the_content();
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	echo '
		<div class="clear"></div>
		<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

/*
 * Custom shortcode to Box
 */
function shortcode_box() {
	$web = get_site_url() . '/';
	$html = '
		<div class="box">
			<p class="text-center"><a href="' . $web . 'testimonios-de-nuestra-gestion/"><img src="' . $web . 'wp-content/uploads/Block-1-1.png" title="Testimonios" alt="Testimonios"></a></p>
			<p class="text-center"><a href="' . $web . 'testimonios-de-nuestra-gestion/"><span class="text_1">TESTIMONIOS</span></a></p>
			<p class="text-center"><a href="' . $web . 'testimonios-de-nuestra-gestion/"><span class="text_2">DE NUESTRA GESTIÓN</span></a></p>
		</div>
		<div class="box">
			<p class="text-center"><a href="' . $web . 'proyecto-integral/"><img src="' . $web . 'wp-content/uploads/Block-1-2.png" title="Proyecto" alt="Proyecto"></a></p>
			<p class="text-center"><a href="' . $web . 'proyecto-integral/"><span class="text_1">PROYECTO</span></a></p>
			<p class="text-center"><a href="' . $web . 'proyecto-integral/"><span class="text_2">INTEGRAL</span></a></p>
		</div>
		<div class="box">
			<p class="text-center"><a href="' . $web . 'eventos-al-dia/"><img src="' . $web . 'wp-content/uploads/Block-1-3.png" title="Eventos" alt="Eventos"></a></p>
			<p class="text-center"><a href="' . $web . 'eventos-al-dia/"><span class="text_1">EVENTOS</span></a></p>
			<p class="text-center"><a href="' . $web . 'eventos-al-dia/"><span class="text_2">AL DÍA</span></a></p>
		</div>
		<div class="box">
			<p class="text-center"><span class="text_3">Síguenos</span></p>
			<p class="text-center">
				<a href="https://www.facebook.com/integralcolegio/" target="_blank"><img src="http://dev03.amapolazul.com/colegio-integral-baquerizo-murillo/wp-content/uploads/Logo-Facebook.png" title="Facebook" alt="Facebook"></a>
				<a href="https://www.instagram.com/integralcolegio/" target="_blank"><img src="http://dev03.amapolazul.com/colegio-integral-baquerizo-murillo/wp-content/uploads/Logo-Instagram.png" title="Instagram" alt="Instagram"></a>
			</p>
		</div>
	';
	return $html;
}
add_shortcode( 'box', 'shortcode_box' );

/*
 * Custom shortcode to Menu
 */
function shortcode_menu() {
	$web = get_site_url() . '/';
	$html = '
		<div class="box">
			<ul>
				<li>
					<p class="text-center"><a href="' . $web . '"><img src="' . $web . 'wp-content/uploads/Menu-1.png" title="Inicio" alt="Inicio"></a></p>
					<p class="text-center"><a href="' . $web . '"><span class="text_1">INICIO</span></a></p>
				</li>
				<li>
					<p class="text-center"><a href="' . $web . 'por-que-integral/"><img src="' . $web . 'wp-content/uploads/Menu-2.png" title="¿Por Qué Integral?" alt="¿Por Qué Integral?"></a></p>
					<p class="text-center"><a href="' . $web . 'por-que-integral/"><span class="text_1">¿POR QUÉ INTEGRAL?</span></a></p>
				</li>
				<li>
					<p class="text-center"><a href="' . $web . 'que-nos-diferencia/"><img src="' . $web . 'wp-content/uploads/Menu-3.png" title="¿Qué Nos Diferencia?" alt="¿Qué Nos Diferencia?"></a></p>
					<p class="text-center"><a href="' . $web . 'que-nos-diferencia/"><span class="text_1">¿QUÉ NOS DIFERENCIA?</span></a></p>
				</li>
				<li>
					<p class="text-center"><a href="' . $web . 'programas-academicos/"><img src="' . $web . 'wp-content/uploads/Menu-4.png" title="Programas Académicos" alt="Programas Académicos"></a></p>
					<p class="text-center"><a href="' . $web . 'programas-academicos/"><span class="text_1">PROGRAMAS ACADÉMICOS</span></a></p>
				</li>
				<li>
					<p class="text-center"><a href="' . $web . 'actividades/"><img src="' . $web . 'wp-content/uploads/Menu-5.png" title="Actividades" alt="Actividades"></a></p>
					<p class="text-center"><a href="' . $web . 'actividades/"><span class="text_1">ACTIVIDADES</span></a></p>
				</li>
				<li>
					<p class="text-center"><a href="' . $web . 'testimonios-de-nuestra-gestion/"><img src="' . $web . 'wp-content/uploads/Menu-6.png" title="Testimonios" alt="Testimonios"></a></p>
					<p class="text-center"><a href="' . $web . 'testimonios-de-nuestra-gestion/"><span class="text_1">TESTIMONIOS</span></a></p>
				</li>
			</ul>
		</div>
	';
	return $html;
}
add_shortcode( 'menu', 'shortcode_menu' );