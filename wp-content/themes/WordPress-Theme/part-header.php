<!-- Begin Top -->
	<!--<section class="top_0 show-for-small-only" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
	</section>-->
	<section class="top_1" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
				<div class="show-for-small-only"><?php dynamic_sidebar( 'contactenos_movil' ); ?></div>
			</div>
		</div>
	</section>
	<section class="top_2" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'top_2' ); ?>
			</div>
		</div>
	</section>
	<section class="top_3 wow bounceInUp show-for-small-only" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-6 columns">
				<?php dynamic_sidebar( 'proceso_de_admision' ); ?>
			</div>
			<div class="small-6 columns">
				<?php dynamic_sidebar( 'contactenos' ); ?>
			</div>
		</div>
	</section>
	<!--<section class="top wow bounceInUp" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-10 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( '25_years' ); ?>
			</div>
		</div>
	</section>-->
<!-- End Top -->